from django.db import models

class Candidates(models.Model):
	name = models.CharField(max_length=100)
	totalExperience = models.IntegerField(default=0)

	def __str__(self):
		return self.name

class WorkExperince(models.Model):
	name = models.ForeignKey(Candidates, on_delete=models.CASCADE, default="")
	start = models.CharField(max_length=20)
	end = models.CharField(max_length=20)

	def __str__(self):
		return self.name.name
