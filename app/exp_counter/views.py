from django.shortcuts import render
from django.views import View
from django.http import JsonResponse, HttpResponse
import json
from datetime import datetime
from .models import Candidates, WorkExperince
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from functools import reduce

def countTotalExp(exp):
	starts = [datetime.strptime(exp[0].get('start'), '%b %Y')]
	ends = [datetime.strptime(exp[0].get('end'), '%b %Y')]
	for item in range(1, len(exp)):
		start = datetime.strptime(exp[item].get('start'), '%b %Y')
		end = datetime.strptime(exp[item].get('end'), '%b %Y')
		if ends[-1] > start and ends[-1] < end:
			ends[-1] = end
		elif ends[-1] < start and ends[-1] < end:
			starts.append(start)
			ends.append(end)
	result = [x - y for x, y in zip(ends, starts)]
	return int(reduce(lambda x, y: x + y, result).days / 365)

@method_decorator(csrf_exempt, name='dispatch')
def index(request):
	if request.method == 'GET':
		candidate_set = Candidates.objects.order_by('-totalExperience').values()
		for item in candidate_set:
			exp_set = list(WorkExperince.objects.filter(name_id=item.get('id')).values('start', 'end'))
			item.update({'workExperience': exp_set})
		return JsonResponse({'candidates': list(candidate_set)}, json_dumps_params={'indent': 2})
	elif request.method == 'POST':
		data = json.loads(request.body.decode("utf-8"))
		name = data.get('name')
		exp = data.get('workExperience')
		totalExperience = countTotalExp(exp)

		candidate_data = {
			'name': name,
			'totalExperience': totalExperience
		}

		candidate = Candidates.objects.create(**candidate_data)
		candidate.save()
		for item in exp:
			work_experience_data = {
				'name': candidate,
				'start': item.get('start'),
				'end': item.get('end')
			}
			exp_item = WorkExperince.objects.create(**work_experience_data)
			exp_item.save()
		return JsonResponse({'message': "Candidate has been successfully added."}, status=201)
